def assignment(new_list, i_new, old_list, i_old):
    new_list[i_new] = old_list[i_old]


def merge_sort(list_to_sort_by_merge):
    # check if list contains at least 2 elements
    if (
        len(list_to_sort_by_merge) > 1
    ):
        # Split list into 2 lists
        mid = len(list_to_sort_by_merge) // 2
        left = list_to_sort_by_merge[:mid]
        right = list_to_sort_by_merge[mid:]

        # Recursively call merge_sort until each element is in its own list
        merge_sort(left)
        merge_sort(right)

        # Sort elements of left and right in new list
        pos_left = 0
        pos_right = 0
        pos_list_to_sort = 0

        # compare elements in both lists and choose the smaller one each time
        while pos_left < len(left) and pos_right < len(right):
            if left[pos_left] <= right[pos_right]:
                assignment(new_list=list_to_sort_by_merge, i_new=pos_list_to_sort, old_list=left, i_old=pos_left)
                pos_left += 1
            else:
                assignment(new_list=list_to_sort_by_merge, i_new=pos_list_to_sort, old_list=right, i_old=pos_right)
                pos_right += 1
            pos_list_to_sort += 1

        # add remaining elements of sublists to new list
        while pos_left < len(left):
            list_to_sort_by_merge[pos_list_to_sort] = left[pos_left]
            pos_left += 1
            pos_list_to_sort += 1

        while pos_right < len(right):
            list_to_sort_by_merge[pos_list_to_sort] = right[pos_right]
            pos_right += 1
            pos_list_to_sort += 1


# Plot list and sorted list
import matplotlib.pyplot as plt

my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
list_indices = range(len(my_list))
plt.plot(list_indices, my_list, 'o')
plt.xlabel("list index")
plt.ylabel("list entry")
plt.title("unsorted list")
plt.show()
merge_sort(my_list)
list_indices = range(len(my_list))
plt.plot(list_indices, my_list, 'o')
plt.xlabel("list index")
plt.ylabel("list entry")
plt.title("sorted list")
plt.show()
